# commit-msg-checker


## Execution step 

 Step 1. Clone the git repo 
          
         >  git clone git@gitlab.com:Khajan/commit-msg-checker.git
         

 Step 2. Provide execute option to the scripts 
          
         >  sudo chmod +x git_commit_checker.sh
         

 Step 3. Run the script ( git_commit_checker.sh )
        
         >  ./git_commit_checker.sh 
         
 Provide you public git repo [ GitHub / GitLab ]  to scan for commit message
         
         >  GIT_REPO_URL:  git@gitlab.com:Username/git_repo.git
         
 Enter you GIT working space 
        
         >  Enter workspace directory : workspace
         
-  Script will scan through your Git repo logs and find out the commit messages withe follwing pattern convention. 

-  Pattern is written in ( regex ) template. For now it will scan for JIRA pattern.
 Example pattern : [JIRA-12] Rest part of commit message  
         
        >  sudo vim regex
        
  Edit the regex file with you own regular expression to set commit message convention 

  
 If no commit message found in all branches with JIRA standard. It will ask to set hook globally/ locally

         ```
         > Globally [ G/g ] : To set JIRA convention for all the git project in future. 
         > Locally [ L/l ] : TO set JIRA convention for this particular project
         ```
##### -  List of all the remote branches are saved in ALL_BRANCHES file.

