#!/bin/bash 

source regex

echo "*******************************************"

echo 
read -p 'GIT REPO URL : ' git_url
#read -p 'PATTERN to FIND in commit message( e.g. JIRA- )  : ' PATTERN
read -p 'Enter workspace directory : ' GIT_DIR


#################### Set hook for incoming pattern globally ########################

function set_hook_commit_globally()
 {
       git config --global init.templatedir '~/.git-templates'
       if [ ! -d "~/.git-templates/hooks" ]; then
          sudo  mkdir -p ~/.git-templates/hooks
       fi
       
       sudo cp ../commit-msg  ~/.git-templates/hooks
       sudo chmod a+x ~/.git-templates/hooks/commit-msg
 }


################# Set hook for incoming pattern locally ################ 

function set_hook_commit_locally()
 {  
      if [ ! -d "${GIT_DIR}/.git/hooks" ]; then
          sudo  mkdir -p ${GIT_DIR}/.git/hooks
      fi
      sudo cp ../commit-msg ${GIT_DIR}/.git/hooks/
      sudo chmod a+x ${GIT_DIR}/.git/hooks/commit-msg
      git init
 }


######### Clone the git repo #################

rm -rf ${GIT_DIR}/
git clone $git_url ${GIT_DIR} > /dev/null 2>&1
cd ${GIT_DIR}

 
#################### Function to get all branches of remote repo ##################

function get_branches()
 {
     git branch -r | grep -v '\->' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done > /dev/null 2>&1
     git branch | awk  '{if($1 != "*" ) { print $1; } else {print $2; } }'  > ../ALL_BRANCHES
     echo -e "\n ********** All Branches *****"
     cat ../ALL_BRANCHES
 }


get_branches

########### Function to print commit messges branches wise ######################


function print_commint_of_pattern()
 {
     input="../ALL_BRANCHES"
     while IFS= read -r line
     do
        echo -e "\n\t ************* $line *****************" 

        git checkout $line > /dev/null 2>&1
        git log --grep=$1 --pretty=format:"%h - %an, %ar : %s"

     done < "$input"
 }


print_commint_of_pattern ${PATTERN1}

#PAT=^\[.*\JIRA-[0-9].*\]

########## Count the total number of commit message based on pattern  "^\[.*JIRA.*\]"  ############  

function pattern_based_commit_count()
 {
     PATTERN_COUNT=`git log  --all --grep=$1 --pretty=oneline | wc -l`
     echo  -e "\n\t Total pattern found  : $PATTERN_COUNT"


     if [[ "$PATTERN_COUNT" -gt 2 ]] 
     then
         echo -e "Pattern found: $PATTERN_COUNT"
     else
         
         echo -e "No commit found like [JIRA-123] : $PATTERN_COUNT"
         echo -e "\nWant to follow convention like -   [JIRA-123] Body part\n"
         
         while true; do
             read -p "To set hook globally/locally enter [g/l]:" option 
             case $option in
             [Gg]* ) echo "Setting hook globally" 
                     $(set_hook_commit_globally); break;;
 
             [Ll]* ) echo "Setting hook for current project"
                     $(set_hook_commit_locally); break;;
 
             * ) echo "Please Type [g/l]"; break;;
             
             esac
        done
     fi
 }

pattern_based_commit_count ${PATTERN1}


